﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using exo2;

namespace testsexo2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAireCarre()
        {
            int annee1 = 1999;
            Assert.AreEqual(20, Program.AireCarre(annee1));
            int annee2 = 2015;
            Assert.AreEqual(4, Program.AireCarre(annee2));
            int annee3 = 1946;
            Assert.AreNotEqual(8000, Program.AireCarre(annee3));
        }
    }
}
