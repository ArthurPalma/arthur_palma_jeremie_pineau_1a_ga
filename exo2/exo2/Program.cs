﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo2
{
    public class Program
    {
        public static int AireCarre(int annee)
        {
            int agecalculé;
            agecalculé = 2019 - annee;
            return agecalculé;
        }
        static void Main(string[] args)
        {
            string annee_as_string;
            int annee, age;
            Console.Write("saisissez votre année de naissance : ");
            annee_as_string = Console.ReadLine();
            int.TryParse(annee_as_string, out annee);
            age = AireCarre(annee);
            if (age >= 18)
            {
                Console.Write("la personne est majeure ( {0} ans )", age);
                Console.ReadKey();
            }
            else
            {
                Console.Write("la personne est mineure ( {0} ans )", age);
                Console.ReadKey();
            }
        }
    }
}
