﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Exercice2;

namespace Testexercice2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestVerification()
        {
            int[] tableau = { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };
            int nbchoisi;
            nbchoisi = 4;
            bool verif = Program.Verification(tableau, nbchoisi);
            Assert.AreEqual(true, Program.Verification(tableau, nbchoisi));
            nbchoisi = 3;
            verif = Program.Verification(tableau, nbchoisi);
            Assert.AreEqual(false, Program.Verification(tableau, nbchoisi));
        }
    }
}
