﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice2
{
    public class Program
    {
        public static int Remplissage(int[] tab)
        {
            /* Incrémente le tableau entré en paramètre */
            string stock_as_string;
            int iteration, stock;
            for (iteration = 0; iteration <= 9; iteration++)
            {
                Console.Write("saisissez la valeur {0} : \n", iteration + 1);
                stock_as_string = Console.ReadLine();
                int.TryParse(stock_as_string, out stock);
                tab[iteration] = stock;
            }
            return tab[10];
        }

        public static bool Verification(int[] tab, int nbchoisi)
        {
            /* Vérifie que le tableau contient la valeur choisie */
            int iteration;
            for (iteration = 0; iteration <= 9; iteration++)
            {
                if (nbchoisi == tab[iteration])
                {
                    return true;
                }
            }
            return false;
        }
        static void Main(string[] args)
        {
            string choix_as_string;
            int[] tableau = new int[20];
            int choix, iteration;

            Remplissage(tableau);
            Console.Write("saisissez un nombre : \n");
            choix_as_string = Console.ReadLine();
            int.TryParse(choix_as_string, out choix);
            Verification(tableau, choix);
            for (iteration = 0; iteration <= 9; iteration++)
            {
                if (choix == tableau[iteration])
                {
                    Console.Write("{0} est  la {1} eme valeur saisie : \n", choix, iteration + 1);
                    Console.ReadKey();
                }
            }

            if (Verification(tableau, choix) == false)
            {
                Console.Write("La valeur entrée n'est pas dans le tableau \n");
                Console.ReadKey();
            }
        }
    }
}
