﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Exercice1;

namespace Testexercice1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMajuscule()
        {
            char test1 = 'I';
            Assert.AreEqual(true, Program.Majuscule(test1));
            char test2 = 'A';
            Assert.AreEqual(true, Program.Majuscule(test2));
            char test3 = 'Z';
            Assert.AreEqual(true, Program.Majuscule(test3));
            char test4 = 'r';
            Assert.AreEqual(false, Program.Majuscule(test4));

        }
        [TestMethod]
        public void TestPoint()
        {
            char test1 = '.';
            Assert.AreEqual(true, Program.Point(test1));
            char test2 = 'r';
            Assert.AreEqual(false, Program.Point(test2));

        }
    }
}
