﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice1
{
    public class Program
    {
        public static bool Majuscule(char first_letter)
        {
            /* Vérifie que la phrase commence par une majuscule */
            if (first_letter >= 'A' && first_letter <= 'Z')
            {
                return true;
            }
            return false;
        }
        public static bool Point(char last_letter)
        {
            /* Vérifie que la phrase se termine par un point */
            if (last_letter == '.')
            {
                return true;
            }
            return false;
        }
        static void Main(string[] args)
        {
            Console.Write("Saisissez une phrase :");
            string phrase = Console.ReadLine();
            int taille = phrase.Length;
            char first_letter = phrase[0];
            char last_letter = phrase[taille - 1];

            if (taille > 0)
            {
                Console.WriteLine("Vous avez saisi : {0}", phrase);
                if (Majuscule(first_letter) == true)
                {
                    Console.WriteLine("Cette phrase commence par une majuscule");
                }
                if (Point(last_letter) == true)
                {
                    Console.WriteLine("Cette phrase se termine par un point");
                }
                Console.ReadKey();
            }
        }
    }
}
